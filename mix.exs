defmodule ManganeseCoreKit.MixProject do
  use Mix.Project

  def project do
    [
      app: :manganese_core_kit,
      version: "0.1.6",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [ :logger ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # ExDoc
      { :ex_doc, "~> 0.19", only: :dev, runtime: false }
    ]
  end

  defp package do
    [
      name: "manganese_core_kit",
      description: "Library for common web application data types in Elixir",
      licenses: [ "Apache 2.0" ],
      links: %{
        "GitLab" => "https://gitlab.com/manganese/core-kit/core-kit-elixir"
      },
      maintainers: [
        "Samuel Goodell"
      ]
    ]
  end
end
