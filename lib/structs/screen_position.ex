defmodule Manganese.CoreKit.Structs.ScreenPosition do
  @moduledoc """

  """

  alias Manganese.CoreKit.Structs

  @typedoc """

  A screen position.

  """
  @type t :: %Structs.ScreenPosition{
    x: float,
    y: float
  }
  defstruct [
    :x,
    :y
  ]

  # Deserialization

  @doc """

  Deserialize a screen position from a map.

  """
  @spec from_map(t_external) :: t
  def from_map(%{
    "x" => x,
    "y" => y
  }) do
    %Structs.ScreenPosition{
      x: x,
      y: y
    }
  end


  # Serialization
  @type t_external :: map

  @doc """

  Serialize a screen position to a map.

  """
  @spec to_map(t) :: t_external
  def to_map(%Structs.ScreenPosition{
    x: x,
    y: y
  }) do
    %{
      "x" => x,
      "y" => y
    }
  end

  # Ecto type
  @behaviour Ecto.Type

  @doc """

  The PostgreSQL type used to represent a screen position.

  """
  @impl Ecto.Type
  def type, do: :map

  @impl Ecto.Type
  def cast(%Structs.ScreenPosition{} = screen_position), do: { :ok, screen_position }
  def cast(%{} = map), do: { :ok, from_map map }
  def cast(nil), do: { :ok, nil }
  def cast(_), do: :error

  @impl Ecto.Type
  def load(%{} = map), do: { :ok, from_map map }
  def load(nil), do: { :ok, nil }
  def load(_), do: :error

  @doc false
  def dump(%Structs.ScreenPosition{} = screen_position), do: { :ok, to_map screen_position }
  def dump(nil), do: { :ok, nil }
  def dump(_), do: :error
end