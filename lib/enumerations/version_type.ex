defmodule Manganese.CoreKit.Enumerations.VersionType do
  @moduledoc """

  """

  @typedoc """

  """
  @type t ::
    :major |
    :minor |
    :patch

  # Deserialization

  @doc """

  """
  @spec from_integer(t_external) :: t
  def from_integer(integer) do
    %{
      3 => :major,
      2 => :minor,
      1 => :patch
    }[integer]
  end


  # Serialization

  @typedoc """

  """
  @type t_external :: non_neg_integer

  @doc """

  """
  def to_integer(value) do
    %{
      major: 3,
      minor: 2,
      patch: 1
    }[value]
  end


  # Ecto type
  @behaviour Ecto.Type

  @doc """

  The PostgreSQL primitive type used to represent a version type.

  """
  @impl true
  def type, do: :integer

  @doc false
  @impl true
  def cast(value) when is_atom(value), do: { :ok, value }
  def cast(integer) when is_integer(integer), do: { :ok, from_integer integer }
  def cast(nil), do: { :ok, nil }
  def cast(_), do: :error

  @doc false
  @impl true
  def load(integer) when is_integer(integer), do: { :ok, from_integer integer }
  def load(nil), do: { :ok, nil }
  def load(_), do: :error

  @doc false
  @impl true
  def dump(value) when is_atom(value), do: { :ok, to_integer value }
  def dump(nil), do: { :ok, nil }
  def dump(_), do: :error
end