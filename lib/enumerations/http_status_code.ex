defmodule Manganese.CoreKit.Enumerations.HttpStatusCode do
  @moduledoc """

  """

  @typedoc """

  """
  @type t ::
    # 100
    :continue |
    :switching_protocols |

    # 200
    :ok |
    :created |
    :accepted |
    :non_authoritative_information |
    :no_content |
    :reset_content |
    :partial_content |

    # 300
    :ambiguous |
    :multiple_choices |
    :moved |
    :moved_permanently |
    :redirect |
    :found |
    :redirect_method |
    :see_other |
    :not_modified |
    :use_proxy |
    :unused |
    :redirect_keep_verb |
    :temporary_redirect |

    # 400
    :bad_request |
    :unauthorized |
    :payment_required |
    :forbidden |
    :not_found |
    :method_not_allowed |
    :not_acceptable |
    :proxy_authentication_required |
    :request_timeout |
    :conflict |
    :gone |
    :length_required |
    :precondition_failed |
    :request_entity_too_large |
    :request_uri_too_long |
    :unsupported_media_type |
    :request_range_not_satisfiable |
    :expectation_failed |
    :im_a_teapot |
    :unprocessable_entity |
    :upgrade_required |

    # 500
    :error |
    :internal_server_error |
    :not_implemented |
    :bad_gateway |
    :service_unavailable |
    :gateway_timeout |
    :http_version_not_supported


  # Deserialization

  @doc """

  """
  @spec to_integer(atom) :: pos_integer
  def to_integer(atom) do
    %{
      # 100
      continue: 100,
      switching_protocols: 101,

      # 200
      ok: 200,
      created: 201,
      accepted: 202,
      non_authoritative_information: 203,
      no_content: 204,
      reset_content: 205,
      partial_content: 206,

      # 300
      ambiguous: 300,
      multiple_choices: 300,
      moved: 301,
      moved_permanently: 301,
      redirect: 302,
      found: 302,
      redirect_method: 303,
      see_other: 303,
      not_modified: 304,
      use_proxy: 305,
      unused: 306,
      redirect_keep_verb: 307,
      temporary_redirect: 307,

      # 400
      bad_request: 400,
      unauthorized: 401,
      payment_required: 402,
      forbidden: 403,
      not_found: 404,
      method_not_allowed: 405,
      not_acceptable: 406,
      proxy_authentication_required: 407,
      request_timeout: 408,
      conflict: 409,
      gone: 410,
      length_required: 411,
      precondition_failed: 412,
      request_entity_too_large: 413,
      request_uri_too_long: 414,
      unsupported_media_type: 415,
      request_range_not_satisfiable: 416,
      expectation_failed: 417,
      im_a_teapot: 418,
      unprocessable_entity: 422,
      upgrade_required: 426,

      # 500
      error: 500,
      internal_server_error: 500,
      not_implemented: 501,
      bad_gateway: 502,
      service_unavailable: 503,
      gateway_timeout: 504,
      http_version_not_supported: 505
    }[atom]
  end


  # Serialization

  @typedoc """

  """
  @type t_external :: pos_integer

  @doc """

  """
  @spec from_integer(pos_integer) :: t
  def from_integer(integer) do
    %{
      # 100
      100 => :continue,
      101 => :switching_protocols,

      # 200
      200 => :ok,
      201 => :created,
      202 => :accepted,
      203 => :non_authoritative_information,
      204 => :no_content,
      205 => :reset_content,
      206 => :partial_content,

      # 300
      300 => :ambiguous,
      301 => :moved,
      302 => :redirect,
      303 => :redirect_method,
      304 => :not_modified,
      305 => :use_proxy,
      306 => :unused,
      307 => :redirect_keep_verb,

      # 400
      400 => :bad_request,
      401 => :unauthorized,
      402 => :payment_required,
      403 => :forbidden,
      404 => :not_found,
      405 => :method_not_allowed,
      406 => :not_acceptable,
      407 => :proxy_authentication_required,
      408 => :request_timeout,
      409 => :conflict,
      410 => :gone,
      411 => :length_required,
      412 => :precondition_failed,
      413 => :request_entity_too_large,
      414 => :request_uri_too_long,
      415 => :unsupported_media_type,
      416 => :request_range_not_satisfiable,
      417 => :expectation_failed,
      418 => :im_a_teapot,
      422 => :unprocessable_entity,
      426 => :upgrade_required,

      # 500
      500 => :internal_server_error,
      501 => :not_implemented,
      502 => :bad_gateway,
      503 => :service_unavailable,
      504 => :gateway_timeout,
      505 => :http_version_not_supported
    }[integer]
  end
end