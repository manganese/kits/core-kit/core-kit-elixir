defmodule Manganese.CoreKit.Enumerations.HttpMethod do
  @moduledoc """

  """

  @typedoc """

  """
  @type t ::
    :options |
    :get |
    :patch |
    :post |
    :put |
    :delete


  # Deserialization

  @doc """

  """
  @spec from_string(String.t) :: t
  def from_string(string) do
    %{
      "OPTIONS" => :options,
      "GET"     => :get,
      "PATCH"   => :patch,
      "POST"    => :post,
      "PUT"     => :put,
      "DELETE"  => :delete
    }[string]
  end


  # Serialization

  @typedoc """

  """
  @type t_external :: String.t

  @doc """

  """
  @spec to_string(t) :: String.t
  def to_string(value) do
    %{
      options: "OPTIONS",
      get:     "GET",
      patch:   "PATCH",
      post:    "POST",
      put:     "PUT",
      delete:  "DELETE"
    }[value]
  end
end