defmodule Manganese.CoreKitTest do
  alias Manganese.CoreKit

  use ExUnit.Case

  doctest Manganese.CoreKit

  # Is uppercase character
  describe "`Manganese.CoreKit.is_uppercase_character/1`" do
    test "it returns `true` when the character is an uppercase letter" do
      character = "A"
      result = CoreKit.is_uppercase_character character

      assert result == true
    end

    test "it returns `false` when the character is a lowercase letter" do
      character = "a"
      result = CoreKit.is_uppercase_character character

      assert result == false
    end

    test "it returns `false` when the character is not a letter" do
      character = "-"
      result = CoreKit.is_uppercase_character character

      assert result == false
    end
  end

  # Is lowercase character
  describe "`Manganese.CoreKit.is_lowercase_character/1`" do
    test "it returns `true` when the character is a lowercase letter" do
      character = "b"
      result = CoreKit.is_lowercase_character character

      assert result == true
    end

    test "it returns `false` when the character is an uppercase letter" do
      character = "B"
      result = CoreKit.is_lowercase_character character

      assert result == false
    end

    test "it returns `false` when the character is not a letter" do
      character = "_"
      result = CoreKit.is_lowercase_character character

      assert result == false
    end
  end

  # Underscore key
  describe "`Manganese.CoreKit.underscore_key/1`" do
    test "it underscores a non-numeric key correctly" do
      string = "indigoVioletBlue"
      result = CoreKit.underscore_key string

      assert result == "indigo_violet_blue"
    end

    test "it underscores a numeric key correctly" do
      string = "asset2dImport"
      result = CoreKit.underscore_key string

      assert result == "asset_2d_import"
    end

    test "it converts an atom to a string and underscores it correctly" do
      atom = :indigoVioletBlue
      result = CoreKit.underscore_key atom

      assert result == "indigo_violet_blue"
    end

    test "it converts an integer to a string and underscores it correctly" do
      integer = 16
      result = CoreKit.underscore_key integer

      assert result == "16"
    end
  end

  # Camelize key
  describe "`Manganese.CoreKit.camelize_key/1`" do
    test "it camelizes a non-numeric key correctly" do
      string = "indigo_violet_blue"
      result = CoreKit.camelize_key string

      assert result == "indigoVioletBlue"
    end

    test "it camelizes a numeric key correctly" do
      string = "asset_2d_import"
      result = CoreKit.camelize_key string

      assert result == "asset2dImport"
    end

    test "it converts an atom to a string and camelizes it correctly" do
      atom = :indigo_violet_blue
      result = CoreKit.camelize_key atom

      assert result == "indigoVioletBlue"
    end

    test "it converts an integer to a string and camelizes it correctly" do
      integer = 16
      result = CoreKit.camelize_key integer

      assert result == "16"
    end
  end
end